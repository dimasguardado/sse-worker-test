EventSource Availability Test
=============================

It looks like the EventSource API (Server-Sent Events) is not available from
within a WebWorker in FireFox. This project will demonstrate the availability
of EventSource in different locations in your favorite browser.

Though the MDN docs do not directly mention the unavailability of EventSource
in a Worker, it is not explicitly listed among the
[Functions available to workers](https://developer.mozilla.org/en-US/docs/Web/Guide/Needs_categorization/Functions_available_to_workers?redirect=no).

I had a pretty hard time finding much mention of this anywhere on the internet,
so I figured working software (or otherwise) would be decent enough
documentation.

Running the test
----------------

Fire up the provided server in Node:

```bash
$ node server.js
```

Launch the [test page](http://localhost:4000/) in your browser.

Presto?
