var http = require('http'),
url = require('url'),
path = require('path'),
fs = require('fs');

var mimeTypes = {
    "html": "text/html",
    "js": "text/javascript"
};

var workingDir = process.cwd();
console.log("Working Dir: " + workingDir);
var publicDir = path.join(workingDir, "/public");
console.log("Public Dir: " + publicDir);

http.createServer(function(req, res) {

    var uri = url.parse(req.url).pathname;
    var filename = path.join(publicDir, uri);

    if (filename.lastIndexOf('/') === (filename.length - 1)) {
        filename = path.join(filename, "index.html");
    }

    console.log("Request URL: " + req.url);
    console.log("Request URI: " + uri);
    console.log("Filename: " + filename);

    fs.exists(filename, function(exists) {

        if(!exists) {
            console.log("not exists: " + filename);
            res.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8'});
            res.write('404 Not Found\n');
            res.end();
            return;
        }

        var mimeType = mimeTypes[path.extname(filename).split(".")[1]]
            + "; charset=utf-8";
        res.writeHead(200, {'Content-Type': mimeType});

        var fileStream = fs.createReadStream(filename);
        fileStream.pipe(res);

    });

}).listen(4000);

console.log('Server started at port 4000. Listening...')
