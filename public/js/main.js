function setResult(id, result) {
    document.getElementById(id)
        .querySelector('.result')
        .innerHTML = '' + result;
}

var eventSourceAvailable = (typeof EventSource) !== 'undefined';

setResult('main-sse', eventSourceAvailable);

var workerAvailable = (typeof Worker) !== 'undefined';

if (workerAvailable) {
    var worker = new Worker('js/worker.js');
    worker.onmessage = function(e) {
        setResult('worker-sse', e.data === 'true');
    }
} else {
    setResult('worker-sse', workerAvailable);
}

setResult('main-worker', workerAvailable);
